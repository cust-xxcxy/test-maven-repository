<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <artifactId>eos-plat-common</artifactId>
    <groupId>com.tmobile.eitss.eos.common</groupId>
    <version>4.0.9</version>
    <name>eos-plat-common</name>
    <packaging>jar</packaging>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.5.1.RELEASE</version>
        <relativePath />  <!-- lookup parent from repository -->
    </parent>
    
    <properties>
        <java.version>1.8</java.version>
        <apache.httpcore.version>4.4.5</apache.httpcore.version>
        <apache.httpclient.version>4.5.2</apache.httpclient.version>
		<apache.commons.io.version>2.3</apache.commons.io.version> 
		<apache.commons.lang3.version>3.4</apache.commons.lang3.version>
        <surefire.version>2.19.1</surefire.version>
        <slf4j.version>1.7.21</slf4j.version>
        <gson.version>2.7</gson.version>
        <java.version>1.8</java.version>
        <joda.time.version>2.3</joda.time.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <username></username>
        <password></password>
        <commons.collections.version>3.2.2</commons.collections.version>
        <commons.codec.version>1.10</commons.codec.version>
        <aws.java.sdk.kinesis.version>1.11.18</aws.java.sdk.kinesis.version>
        <okhttp-version>2.7.5</okhttp-version>
        <lombok.version>1.16.20</lombok.version>
        <jjwt.version>0.9.0</jjwt.version>
    </properties>

    <dependencies>
        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter -->
       
        <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot</artifactId>
		</dependency>
		
		<!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
		<dependency>
		    <groupId>org.projectlombok</groupId>
		    <artifactId>lombok</artifactId>
		    <version>${lombok.version}</version>
		    <scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-aop</artifactId>
		</dependency>
				
		<dependency>
		    <groupId>org.springframework.boot</groupId>
		    <artifactId>spring-boot-starter-data-redis</artifactId>
		</dependency>

		<!-- https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt -->
		<dependency>
		    <groupId>io.jsonwebtoken</groupId>
		    <artifactId>jjwt</artifactId>
		    <version>${jjwt.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.ws</groupId>
			<artifactId>spring-ws-core</artifactId>
			<version>2.4.0.RELEASE</version>
		</dependency>
		 <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${apache.commons.lang3.version}</version>
        </dependency>
        
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpcore</artifactId>
            <version>${apache.httpcore.version}</version>
        </dependency>
        
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>${apache.httpclient.version}</version>
        </dependency>
		
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>${apache.commons.io.version}</version>
		</dependency>
		<dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
        </dependency>
        <!-- gson -->
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>${gson.version}</version>
        </dependency>
        <!-- Joda time -->
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>${joda.time.version}</version>
        </dependency>
        
        <!-- https://mvnrepository.com/artifact/commons-collections/commons-collections -->
		<dependency>
		    <groupId>commons-collections</groupId>
		    <artifactId>commons-collections</artifactId>
		    <version>${commons.collections.version}</version>
		</dependency>
		   
		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
			<version>${commons.codec.version}</version>
		</dependency>

		<dependency>
	        <groupId>com.netflix.hystrix</groupId>
	        <artifactId>hystrix-core</artifactId>
	        <version>1.5.9</version>
      </dependency>
      
      <!-- Annotator for skipping log -->
      <dependency>
            <groupId>com.tmobile.eitss.eos</groupId>
            <artifactId>eos-model-annotator</artifactId>
            <version>1.0.11</version>
        </dependency>
		<dependency>
	    	<groupId>com.tmobile.eitss.eos.services</groupId>
	    	<artifactId>eos-oauthmanager-certs-rest-client</artifactId>
	    	<version>1.0.0</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/com.amazonaws/aws-java-sdk-kinesis -->
		<dependency>
    		<groupId>com.amazonaws</groupId>
    		<artifactId>aws-java-sdk-kinesis</artifactId>
    		<version>${aws.java.sdk.kinesis.version}</version>
		</dependency>
		
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-dynamodb</artifactId>
		</dependency>
		<dependency>
   			<groupId>com.tmobile.eitss.cloud</groupId>
    		<artifactId>aws-common</artifactId>
    		<version>1.0.2</version>
    		<scope>compile</scope>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.mockito/mockito-all -->
		<dependency>
		    <groupId>org.mockito</groupId>
		    <artifactId>mockito-all</artifactId>
		    <version>1.10.19</version>
		</dependency>    
		
   		<dependency>
      		<groupId>com.squareup.okhttp</groupId>
      		<artifactId>logging-interceptor</artifactId>
      		<version>${okhttp-version}</version>
    	</dependency>
    	
		<dependency>
			<groupId>com.tmobile.eitss.eos.cache.redis</groupId>
		    <artifactId>eos-redis-cache-utils</artifactId>
		    <version>1.2.4</version>
	    </dependency>
	    
		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-jsr310</artifactId>
		</dependency>
		
		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-joda</artifactId>
			<version>2.9.6</version>
		</dependency>
    </dependencies>
    <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-lang3 -->
    
    <!-- AWS Java Bill of Materials -->
    <dependencyManagement>
	<dependencies>
		<dependency>
			<groupId>com.amazonaws</groupId>
			<artifactId>aws-java-sdk-bom</artifactId>
			<version>1.11.18</version>
			<type>pom</type>
			<scope>import</scope>
		</dependency>
	</dependencies>    
</dependencyManagement>
    
    <build>
        <pluginManagement>
            <plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>org.jfrog.buildinfo</groupId>
										<artifactId>
											artifactory-maven-plugin
										</artifactId>
										<versionRange>
											[2.6.0,)
										</versionRange>
										<goals>
											<goal>publish</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
				
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>2.6</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>2.1.2</version>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.jfrog.buildinfo</groupId>
                <artifactId>artifactory-maven-plugin</artifactId>
                <version>2.6.0</version>
                <inherited>false</inherited>
                <executions>
                    <execution>
                        <id>build-info</id>
                        <goals>
                            <goal>publish</goal>
                        </goals>
                        <configuration>
                            <deployProperties>
                                <gradle>awesome</gradle>
                            </deployProperties>
                            <publisher>
                                <contextUrl>http://artifactory.corporate.t-mobile.com/artifactory</contextUrl>
                                <username>${username}</username>
                                <password>${password}</password>
                                <excludePatterns>*-tests.jar</excludePatterns>
                                <repoKey>eos-release-local</repoKey>
                                <snapshotRepoKey>eos-snapshot-local</snapshotRepoKey>
                            </publisher>
                            <licenses>
                                <autoDiscover>true</autoDiscover>
                                <includePublishedArtifacts>false</includePublishedArtifacts>
                                <runChecks>true</runChecks>
                                <scopes>compile,runtime</scopes>
                                <violationRecipients>build@organisation.com</violationRecipients>
                            </licenses>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <addMavenDescriptor>false</addMavenDescriptor>
                    </archive>
                </configuration>
            </plugin>

        </plugins>
    </build>

    <pluginRepositories>
        <pluginRepository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>central</id>
            <name>bintray-plugins</name>
            <url>https://jcenter.bintray.com</url>
        </pluginRepository>
    </pluginRepositories>

    <repositories>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>central</id>
            <name>bintray</name>
            <url>https://jcenter.bintray.com</url>
        </repository>
        <repository>
			<id>release</id>
			<name>prd-art-jav-2a.corporate.t-mobile.com-release</name>
			<url>http://artifactory.corporate.t-mobile.com/artifactory/eos-release-local</url>
		</repository>
		<repository>
			<id>snapshot</id>
			<name>prd-art-jav-2a.corporate.t-mobile.com-snapshot</name>
			<url>http://artifactory.corporate.t-mobile.com/artifactory/eos-snapshot-local</url>
		</repository>
    </repositories>
    
    <distributionManagement>
        <repository>
            <id>release</id>
            <name>prd-art-jav-1a.corporate.t-mobile.com-releases</name>
            <url>https://artifactory.corporate.t-mobile.com/artifactory/eos-release-local</url>
        </repository>
        <snapshotRepository>
            <id>snapshots</id>
            <name>prd-art-jav-1a.corporate.t-mobile.com-snapshots</name>
            <url>https://artifactory.corporate.t-mobile.com/artifactory/eos-snapshot-local</url>
        </snapshotRepository>
    </distributionManagement>
	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
				<version>3.8</version>
				<configuration>
					<skipEmptyReport>false</skipEmptyReport>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>3.0.4</version>
				<configuration>
					<effort>Max</effort>
					<failOnError>false</failOnError>
					<threshold>Low</threshold>
					<xmlOutput>true</xmlOutput>
					<findbugsXmlOutputDirectory>${project.build.directory}/findbugs</findbugsXmlOutputDirectory>
					<plugins>
						<plugin>
							<groupId>com.h3xstream.findsecbugs</groupId>
							<artifactId>findsecbugs-plugin</artifactId>
							<version>LATEST</version> <!-- Auto-update to the latest stable -->
						</plugin>
					</plugins>
				</configuration>
			</plugin>
		</plugins>
	</reporting>
</project>
