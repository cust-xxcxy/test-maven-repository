<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.tmobile.eitss.cloud</groupId>
    <artifactId>aws-common</artifactId>
    <version>0.0.1</version>

    <properties>
        <java.version>1.8</java.version>
        <spring.boot.version>1.5.1.RELEASE</spring.boot.version>
        <commons-lang3.version>3.4</commons-lang3.version>
        <httpcore.version>4.4.5</httpcore.version>
        <httpclient.version>4.5.2</httpclient.version>
        <surefire.version>2.19.1</surefire.version>
        <slf4j.version>1.7.21</slf4j.version>
        <aws.sdk.version>1.11.163</aws.sdk.version>
        <gson.version>2.7</gson.version>
        <joda.time.version>2.3</joda.time.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <username></username>
        <password></password>
    </properties>

    <dependencies>
        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
            <version>${spring.boot.version}</version>
        </dependency>
        <!-- commons lang3 -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${commons-lang3.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpcore</artifactId>
            <version>${httpcore.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>${httpclient.version}</version>
        </dependency>
        <dependency>
            <groupId>com.amazonaws</groupId>
            <artifactId>aws-java-sdk</artifactId>
            <version>${aws.sdk.version}</version>
        </dependency>
        <!-- gson -->
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>${gson.version}</version>
        </dependency>
        <!-- Joda time -->
        <dependency>
            <groupId>joda-time</groupId>
            <artifactId>joda-time</artifactId>
            <version>${joda.time.version}</version>
        </dependency>
        <dependency>
        	<groupId>org.springframework.boot</groupId>
        	<artifactId>spring-boot-configuration-processor</artifactId>
        	<version>${spring.boot.version}</version>
        	<optional>true</optional>
        </dependency>
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>2.6</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>2.1.2</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.6.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jfrog.buildinfo</groupId>
                <artifactId>artifactory-maven-plugin</artifactId>
                <version>2.6.1</version>
                <inherited>false</inherited>
                <executions>
                    <execution>
                        <id>build-info</id>
                        <goals>
                            <goal>publish</goal>
                        </goals>
                        <configuration>
                            <deployProperties>
                                <gradle>awesome</gradle>
                            </deployProperties>
                            <publisher>
                                <contextUrl>http://artifactory.corporate.t-mobile.com/artifactory</contextUrl>
                                <username>${username}</username>
                                <password>${password}</password>
                                <excludePatterns>*-tests.jar</excludePatterns>
                                <repoKey>eos-release-local</repoKey>
                                <snapshotRepoKey>eos-snapshot-local</snapshotRepoKey>
                            </publisher>
                            <licenses>
                                <autoDiscover>true</autoDiscover>
                                <includePublishedArtifacts>false</includePublishedArtifacts>
                                <runChecks>true</runChecks>
                                <scopes>compile,runtime</scopes>
                                <violationRecipients>build@organisation.com</violationRecipients>
                            </licenses>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <configuration>
                    <archive>
                        <addMavenDescriptor>false</addMavenDescriptor>
                    </archive>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <pluginRepositories>
        <pluginRepository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>central</id>
            <name>bintray-plugins</name>
            <url>http://jcenter.bintray.com</url>
        </pluginRepository>
    </pluginRepositories>

    <repositories>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>central</id>
            <name>bintray</name>
            <url>http://jcenter.bintray.com</url>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>central</id>
            <name>prd-art-jav-1a.corporate.t-mobile.com-releases</name>
            <url>https://artifactory.corporate.t-mobile.com/artifactory/eos-snapshot-local</url>
        </repository>
        <snapshotRepository>
            <id>snapshots</id>
            <name>prd-art-jav-1a.corporate.t-mobile.com-snapshots</name>
            <url>https://artifactory.corporate.t-mobile.com/artifactory/eos-snapshot-local</url>
        </snapshotRepository>
    </distributionManagement>
</project>